# Installation

## zsh

#### To enable completion:

* Run this in the shell:
  compaudit | xargs chmod g-w

## emacs

### Generic config replacements

* config.el
```elisp
(setq user-full-name "Anthony Boetto"
      user-mail-address "INSERT EMAIL HERE")
(setq org-directory "INSERT ORG DIR HERE")
(org-journal-dir "INSERT ORG ROAM DIR HERE")
(deft-directory "INSERT ORG ROAM DIR HERE"))
```

